# asdf-goneovim

Quick install:

```shell
asdf plugin add goneovim https://gitlab.com/dmakovey/asdf-goneovim.git
asdf list all goneovim
asdf install goneovim latest
asdf global goneovim latest
```
